#include <vector>
#include <iostream>
#include <cstring>
#include <cstdlib>

using namespace std;

/* een ceasercijfer met de juiste voorzorg om te zorgen dat alleen alfabet karakters worden aangepast en binnen het alfabet the blijven*/

string ceasarcijfer(string text,int x){
    string cijfer;
    for(unsigned int i =0;i<text.size();i++){
        char tmp = text[i]+x;
        if((int)tmp>=97 && (int)tmp <=122){
            cijfer.push_back(tmp);
        }else if((int)tmp >=65 && (int)tmp <=90){
            cijfer.push_back(tmp);
        }else if((int)text[i]==32||(int)text[i]==44||(int)text[i]==46){ //spaties, kommas en punten behouden
            cijfer.push_back(text[i]);
        }else if((int)tmp> 122) {
            int diff = 122 - (int)text[i]; //het verschil uitrekenen tussen start en eind om te bepalen hoever we nog moeten gaan/
            tmp = 65+((x-diff)-1);
            cijfer.push_back(tmp);
        }else if((int)tmp>90 && (int)tmp<97){  //het verschil uitrekenen tussen start en eind om te bepalen hoever we nog moeten gaan/
            int diff = 90 - (int)text[i];
            tmp = (char)97+((x-diff)-1);
            cijfer.push_back(tmp);
        }
        
    }
    return cijfer;
}
int main(int argc, char* argv[]){
    string invoer;
    if(argc!=2){
        cout << "tool.elf: Deze tool verwacht exact 1 argument"<< endl;
        return 0;
    }
    int x = atoi(argv[1]); 
    if(x<0){
        x *= -1;
    }
    if(x>5){ //maximum van 5 om buiten speciale karakters te komen
        cout << "tool.elf: geef een getal tussen 0 en 5"<< endl;
        return 0;
    }
    while(getline(cin,invoer)){;
        if(invoer == "exit"){
            break;
        }
        string cijfer = ceasarcijfer(invoer,x);
        for(unsigned int i = 0;i<cijfer.size();i++){
            cout << cijfer[i];
        }
        cout << endl;
    }
}