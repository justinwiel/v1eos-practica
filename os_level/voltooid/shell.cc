#include "shell.hh" 
#include <unistd.h>
#include <syscall.h>
#include <iostream>
#include <string>
#include <chrono>
#include <ctime>

int main()
{ std::string input;
  int fd =  syscall(SYS_open, "Prompt", O_RDONLY, 0755);
  char letter[18];
  std::string prompt;
  syscall(SYS_read, fd, letter, 18);
  for(unsigned int i = 0; i<18;i++)
    prompt.push_back(letter[i]);
  while(true)
  { std::cout << prompt << " ";                   // Print het prompt
    std::getline(std::cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file() // ToDo: Implementeer volgens specificatie.
{ std::string filename;
  std::string input;
  std::string inhoud;
  std::cout << "geef een bestandsnaam op: ";
  std::getline(std::cin,filename);
  std::cout << "Geef uw text op: ";
  while(input != "<EOF>"){
	  std::getline(std::cin,input);
	  if(input == "<EOF>"){
		  break;
	  }
	inhoud.append(input);
	inhoud.push_back('\n');
  }
  syscall(SYS_creat, filename.c_str(), 0755);
  int fd =syscall(SYS_open,filename.c_str(),O_RDWR,075);
  syscall(SYS_write,fd,inhoud.c_str(),inhoud.size());
  

}

void list() // ToDo: Implementeer volgens specificatie.
{   int child = syscall(SYS_fork) ;
                                  
    if (child < 0){
      std::cout << "Failed to fork a new process";
      return;
    }else if(child >0){ //als je parent bent wacht tot child klaar is
      syscall(SYS_wait4, child,NULL,NULL,NULL);
    }else if(child == 0){ //als je child bent ga ls uitvoeren
      const char* args[] = {"/bin/ls","-l","-a",NULL};
      syscall(SYS_execve,args[0],args,NULL);
    }
}

void find() 
{     int fd[2];
      syscall(SYS_pipe,&fd);
      std::string input = "";
      std::cout << "Geef een string: ";
      std::getline(std::cin,input);
      int child1 = syscall(SYS_fork);
      if(child1 == 0){//als je child bent doe find .
          const char* args[] = {"/usr/bin/find",".",NULL};
          syscall(SYS_close,fd[0]);
          syscall(SYS_dup2,fd[1],1);
          syscall(SYS_execve,args[0],args,NULL);
      }else if(child1>0){ // als je parent bent maak nog een child
          int child2 = syscall(SYS_fork);
          if(child2==0){ //als je child bent start grep string
            const char* args2[] = {"/bin/grep",input.c_str(),NULL};
            syscall(SYS_close,fd[1]);
            syscall(SYS_dup2,fd[0],0);
            syscall(SYS_execve,args2[0],args2,NULL);
            
          }
          else if(child2>0){  
            syscall(SYS_wait4, child1,NULL,NULL,NULL);
            syscall(SYS_close,fd[0]);
            syscall(SYS_close,fd[1]);
            syscall(SYS_wait4, child2,NULL,NULL,NULL);
          }
      }
      

}

void seek() // ToDo: Implementeer volgens specificatie.
{ std::string x_buffer = "x";
  std::string nul_buffer ="\0";
  auto start_loop = std::chrono::system_clock::now();
  std::string filename1 = "loop";
  syscall(SYS_creat, filename1.c_str(), 0755);
  int fd1 =syscall(SYS_open,filename1.c_str(),O_RDWR,0775);
  syscall(SYS_write,fd1,x_buffer.c_str(),1);
  for(int i = 0;i<5000000;i++){
    syscall(SYS_write,fd1,nul_buffer.c_str(),1);
  }
  syscall(SYS_write,fd1,x_buffer.c_str(),1);
  auto end_loop = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_loop = end_loop - start_loop;
  std::cout << "Time taken for loop: "<<  elapsed_loop.count() << 's' << '\n';

  auto start_seek = std::chrono::system_clock::now();
  std::string filename2 = "seek";
  syscall(SYS_creat, filename2.c_str(), 0755);
  int fd2 =syscall(SYS_open,filename2.c_str(),O_RDWR,0775);
  syscall(SYS_write,fd2,x_buffer.c_str(),1);
  int fd2_seek = syscall(SYS_lseek,fd2,5000000,SEEK_CUR);
  syscall(SYS_write,fd2,x_buffer.c_str(),1);
  auto end_seek = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seek = end_seek - start_seek;
  std::cout << "Time taken for seek: "<<  elapsed_seek.count()<< 's'<< '\n';
}

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
